/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: final
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/
#ifndef GAME_HPP_
#define GAME_HPP_

/*
 * Use this file as a *starting point*.  You may add more classes and other
 * definitions here.
 *
 * We have suggested a couple ideas (Board and Game) for base classes you can
 * use in your design.
 *
 * Implement derived types for the games in "battleschip.h" and "tictactoe.h"
 *
 * You must use "enum GameResult" and "Coord" as defined here, and you must
 * implement derived types called BattleshipGame and TicTacToeGame, each with
 * a public attack_square member function, as you can see from reading
 * play_bs.cpp and play_ttt.cpp.
 */
#include <utility>
#include <map>
#include <vector>
#include <iostream>

enum GameResult {
    RESULT_KEEP_PLAYING, // turn was valid and game is not over yet
    RESULT_INVALID,      // turn was invalid; e.g. attacked square
                         // was attacked previously
    RESULT_STALEMATE,    // game over, neither player wins
    RESULT_PLAYER1_WINS, // game over, player 1 wins
    RESULT_PLAYER2_WINS  // game over, player 2 wins
};

typedef std::pair<int, int> Coord;

class Board {
    std::map<Coord, char> gameBoard;
    int x;
    int y;
public:
    /*
     * Default constructor for Board
     */
    Board()
	{
    	x = 0;
    	y = 0;
	}
    /*
     * Constructs Board with dimensions x by y as a map with coordinates (a, b) where 0 <= a < x and 0 <= b < y
     */
    Board(int x, int y)
    {
    	this->x = x;
    	this->y = y;
    	for(int i = 0; i < x; i++)
    	{
    		for (int k = 0; k < y; k++)
    		{
    			gameBoard[std::make_pair(i,k)] = '-';
    		}
    	}
    }
    /*
     * Returns gameBoard map
     * @return gameBoard map of current game
     */
    std::map<Coord, char> getMap()
	{
    	return gameBoard;
	}
    /*
     * Changes the value at key coor to char c
     * @param coor coordinate on board to add the character to
     * @param c character to be added to specified coordinate on board
     */
    void addAt(Coord coor, char c)
    {
    	gameBoard[coor] = c;
    }
    /*
     * Returns the character at the specified coordinate on the game board
     * @param coor coordinate on gameBoard to get character from
     * @return gameBoard[coor] character at Coord coor on gameBoard
     */
    char get(Coord coor)
    {
    	return gameBoard[coor];
    }

    void print()
    {
    	for (int i = 0; i < y; i++)
    	{
    		for (int k = 0; k < x; k++)
    		{
    			std::cout << gameBoard[std::make_pair(k, i)];
    		}
    		std::cout << '\n';
    	}
    }
};

class Game {
protected:
    int player;
    std::vector<Board> b;
    Board currentBoard;
public:
    /*
     * Default constructor for game
     */
    Game()
	{
    	player = 1;
	}
    /*
     * Constructs a game with a numBoards Boards of x by y dimensions
     */
    Game(int x, int y, int numBoards)
    {
    	for ( int i = 0; i < numBoards; i++ )
    	{
    		b.push_back(Board(x, y));
    	}
    	player = 1;
    	currentBoard = b.at(0);
    }

    /*
     * Return specified player's board
     * @param player player whose board is to be returned
     * @return b board of player
     */
    Board getBoard(int player)
    {
    	return b.at(player - 1);
    }
};



#endif /* GAME_HPP_ */
