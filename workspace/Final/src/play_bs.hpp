/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: final
Phone number: 408-828-6315
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/


#ifndef PLAY_BS_HPP_
#define PLAY_BS_HPP_

#include "battleship.hpp"
#include "ship.hpp"
#include <iostream>
#include <cassert>
#include <vector>
#include <utility>
#include <cctype>
#include <cstdlib>

bool checkValid(std:: string placement);
bool checkEmpty(Coord c, std::vector<Ship> ships);
bool checkValidM(std::string move, std::vector<Ship> ships);
bool checkValid(std::string placement, int size, std::vector<Coord> filled);
std::vector<Ship> placeShips(int player);
void playBattleship(int x);

#endif /* PLAY_BS_HPP_ */
