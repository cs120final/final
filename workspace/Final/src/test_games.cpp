/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: Final
Phone number: 408-828-6315
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/


#include "play_bs.hpp"


typedef std::pair<int, int> Coord;
using namespace std;

void game1(){

	Ship carrier1 = Ship("AIRCRAFT CARRIER", make_pair(1,1), 5, 'v');
	Ship battleship1 = Ship("BATTLESHIP", make_pair(2,1), 4, 'v');
	Ship cruiser1 = Ship("CRUISER", make_pair(3,1), 3, 'v');
	Ship sub1 = Ship("SUBMARINE", make_pair(4,1), 3, 'v');
	Ship destroyer1 = Ship("DESTROYER", make_pair(5,1), 2, 'v');

	Ship carrier2 = Ship("AIRCRAFT CARRIER", make_pair(1,1), 5, 'v');
	Ship battleship2 = Ship("BATTLESHIP", make_pair(2,1), 4, 'v');
	Ship cruiser2 = Ship("CRUISER", make_pair(3,1), 3, 'v');
	Ship sub2 = Ship("SUBMARINE", make_pair(4,1), 3, 'v');
	Ship destroyer2 = Ship("DESTROYER", make_pair(5,1), 2, 'v');

	vector<Ship> player1_ships;
	player1_ships.push_back(carrier1);
	player1_ships.push_back(battleship1);
	player1_ships.push_back(cruiser1);
	player1_ships.push_back(sub1);
	player1_ships.push_back(destroyer1);

	vector<Ship> player2_ships;
	player2_ships.push_back(carrier2);
	player2_ships.push_back(battleship2);
	player2_ships.push_back(cruiser2);
	player2_ships.push_back(sub2);
	player2_ships.push_back(destroyer2);


	  BattleshipGame bsg(player1_ships, player2_ships);

	  //P1's turn
	  GameResult result = bsg.attack_square(make_pair(2, 1)); // P1 HITS P2

	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b11= make_pair(2,1);
	  assert(bsg.getMyShips()[1].getLocs()[b11] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(30, 30)); // P2 invalid move
	  assert(result == RESULT_INVALID);
	  Coord b2= make_pair(2,1);
	  assert(bsg.getShips()[1].getLocs()[b2] == 1);
	  result = bsg.attack_square(make_pair(2, 1)); // P2 HITS P1
	  assert(result == RESULT_KEEP_PLAYING);
	  assert(bsg.getMyShips()[1].getLocs()[b2] == 0);

	  //P1's turn
	  result = bsg.attack_square(make_pair(2, 2)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b12= make_pair(2,2);
	  assert(bsg.getMyShips()[1].getLocs()[b12] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(2, 3)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b13= make_pair(2,3);
	  assert(bsg.getMyShips()[1].getLocs()[b13] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(2, 4)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b14= make_pair(2,4);
	  assert(bsg.getMyShips()[1].getLocs()[b14] == 0);
//
	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(1, 1)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b15= make_pair(1,1);
	  assert(bsg.getMyShips()[0].getLocs()[b15] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(1, 2)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b16= make_pair(1,2);
	  assert(bsg.getMyShips()[0].getLocs()[b16] == 0);
//
	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(1, 3)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b17= make_pair(1,3);
	  assert(bsg.getMyShips()[0].getLocs()[b17] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(1, 4)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b18= make_pair(1,4);
	  assert(bsg.getMyShips()[0].getLocs()[b18] == 0);
//
	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(1, 5)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b19= make_pair(1,5);
	  assert(bsg.getMyShips()[0].getLocs()[b19] == 0);



	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(3, 1)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b110= make_pair(3,1);
	  assert(bsg.getMyShips()[2].getLocs()[b110] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(3, 2)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b111= make_pair(3,2);
	  assert(bsg.getMyShips()[2].getLocs()[b111] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(3, 3)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b112= make_pair(3,3);
	  assert(bsg.getMyShips()[2].getLocs()[b112] == 0);



//
	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(4, 1)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b113= make_pair(4,1);
	  assert(bsg.getMyShips()[3].getLocs()[b113] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(4, 2)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b114= make_pair(4,2);
	  assert(bsg.getMyShips()[3].getLocs()[b114] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(4, 3)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b115= make_pair(4,3);
	  assert(bsg.getMyShips()[3].getLocs()[b115] == 0);


//

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(5, 1)); // P1 HITS P2
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b116= make_pair(5,1);
	  assert(bsg.getMyShips()[4].getLocs()[b116] == 0);

	  //P2's turn
	  result = bsg.attack_square(make_pair(8, 8)); // P2 MISSES P1
	  assert(result == RESULT_KEEP_PLAYING);
	  b2= make_pair(8,8);
	  assert(bsg.getBoard(1).getMap()[b2] == 'o');

	  //P1's turn
	  result = bsg.attack_square(make_pair(5, 2)); // P1 HITS P2
	  assert(result == RESULT_PLAYER1_WINS);



	  cout<< "game1: succesfully checked for wrong inputs: " << endl;

}

void game2(){
	Ship carrier1 = Ship("AIRCRAFT CARRIER", make_pair(1,1), 5, 'v');
	Ship battleship1 = Ship("BATTLESHIP", make_pair(2,1), 4, 'v');
	Ship cruiser1 = Ship("CRUISER", make_pair(3,1), 3, 'v');
	Ship sub1 = Ship("SUBMARINE", make_pair(4,1), 3, 'v');
	Ship destroyer1 = Ship("DESTROYER", make_pair(7,7), 2, 'h');

	Ship carrier2 = Ship("AIRCRAFT CARRIER", make_pair(1,1), 5, 'v');
	Ship battleship2 = Ship("BATTLESHIP", make_pair(2,1), 4, 'v');
	Ship cruiser2 = Ship("CRUISER", make_pair(3,1), 3, 'v');
	Ship sub2 = Ship("SUBMARINE", make_pair(4,1), 3, 'v');
	Ship destroyer2 = Ship("DESTROYER", make_pair(7,7), 2, 'h');

	vector<Ship> player1_ships;
	player1_ships.push_back(carrier1);
	player1_ships.push_back(battleship1);
	player1_ships.push_back(cruiser1);
	player1_ships.push_back(sub1);
	player1_ships.push_back(destroyer1);

	vector<Ship> player2_ships;
	player2_ships.push_back(carrier2);
	player2_ships.push_back(battleship2);
	player2_ships.push_back(cruiser2);
	player2_ships.push_back(sub2);
	player2_ships.push_back(destroyer2);


	BattleshipGame bsg(player1_ships, player2_ships);

	  //P1's turn
	  map<Coord, int> ship = bsg.getShips()[1].getLocs();
	  GameResult result = bsg.moveship(make_pair(2, 1), 'u', 1); // P1 Moves up 1 step
	  assert(result == RESULT_KEEP_PLAYING);
	  Coord b11= make_pair(2,0);
	  map<Coord, int> ship1 = bsg.getShips()[1].getLocs();
	  assert(ship1.find(b11) != ship1.end());
	  cout << bsg.getMyShips()[1].getLocs()[b11];
	  b11= make_pair(2,1);
	  assert(ship1.find(b11) != ship1.end());
	  b11= make_pair(2,2);
	  assert(ship1.find(b11) != ship1.end());
	  b11= make_pair(2,3);
	  assert(ship1.find(b11) != ship1.end());
	  b11= make_pair(2,4);
	  assert(ship1.find(b11) == ship1.end());


	  //P2's turn
	  ship = bsg.getShips()[1].getLocs();
	  result = bsg.moveship(make_pair(2, 1), 'd', 3);// P2 Moves down 3
	  assert(result == RESULT_KEEP_PLAYING);
	  b11= make_pair(2,4);
	  ship1 = bsg.getShips()[1].getLocs();
	  assert(ship1.find(b11) != ship1.end());
	  cout << bsg.getMyShips()[1].getLocs()[b11];
	  b11= make_pair(2,5);
	  assert(ship1.find(b11) != ship1.end());
	  b11= make_pair(2,6);
	  assert(ship1.find(b11) != ship1.end());
	  b11= make_pair(2,1);
	  assert(ship1.find(b11) == ship1.end());

	  //P1's turn
	  ship = bsg.getShips()[4].getLocs();
	  result = bsg.moveship(make_pair(7, 7), 'l', 1); // P1 Moves left 1 step
	  assert(result == RESULT_KEEP_PLAYING);
	  b11= make_pair(6,7);
	  ship1 = bsg.getShips()[4].getLocs();
	  assert(ship1.find(b11) != ship1.end());
	  cout << bsg.getMyShips()[1].getLocs()[b11];
	  b11= make_pair(7,7);
	  assert(ship1.find(b11) != ship1.end());
	  b11= make_pair(8,7);
	  assert(ship1.find(b11) == ship1.end());



	  //P2's turn
	  ship = bsg.getShips()[4].getLocs();
	  result = bsg.moveship(make_pair(7, 7), 'r', 1);// P2 Moves right 3
	  assert(result == RESULT_KEEP_PLAYING);
	  b11= make_pair(8,7);
	  ship1 = bsg.getShips()[4].getLocs();
	  assert(ship1.find(b11) != ship1.end());
	  cout << bsg.getMyShips()[1].getLocs()[b11];
	  b11= make_pair(6,7);
	  assert(ship1.find(b11) == ship1.end());


	  cout<< "game2: succesfully checked for wrong inputs: " << endl;
}

/*
int main()
{
	game1();
	game2();
}
*/
