/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: final
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/
#ifndef SHIP_HPP_
#define SHIP_HPP_

#include <iostream>
#include <vector>

typedef std::pair<int, int> Coord;

class Ship
{
	std::string name;
	std::map<Coord, int> locs;
	int shipSize;
	char orientation;
	Coord front; //left most or top most of ship
	Coord back; //right most or bottom most of ship
public:
	/*
	 * Default constructor for ship
	 */
	Ship()
	{
		orientation = 'h';
		shipSize = 0;
	}
	/*
	 * Ship constructor specifying its coordinate at c, of size size and orientation orientation.
	 */
	Ship(std::string s, Coord c, int size, char orientation)
	{
		this->orientation = orientation;
		name = s;
		shipSize = size;
		if (orientation == 'h')
		{
				front = std::make_pair(c.first,c.second);
				back = std::make_pair(c.first + size - 1,c.second);
			for (int i = 0; i < size; i++)
			{
				locs[std::make_pair(c.first + i, c.second)] = 1;

			}

		}
		else if (orientation == 'v')
		{
			front = std::make_pair(c.first,c.second);
			back = std::make_pair(c.first ,c.second + size - 1);
			for (int i = 0; i < size; i++)
			{
				locs[std::make_pair(c.first, c.second + i)] = 1;
			}
		}
		else
		{
			std::cout << "Invalid orientation" << std::endl;
		}
	}

	/*
	 * Returns map of coordinates corresponding to the ship position
	 * @return locs the map of coordinates at which the ship is at
	 */
	std::map<Coord, int> getLocs()
	{
		return locs;
	}

	/*
	 * Returns the name of the ship (for testing purposes only)
	 * @return name name of the current ship
	 */
	std::string getName()
	{
		return name;
	}

	/*
	 * Returns the size of the ship
	 * @return shipSize size of the ship
	 */
	int size()
	{
		return shipSize;
	}

	/*
	 * Operator overload of == to check if coordinate correlates with ship position
	 * @param c coordinate to check if corresponds with ship position
	 * @return true of coordinate does correspond with ship position, false otherwise
	 */
	bool operator== (Coord c)
	{
		return locs.find(c) != locs.end();
	}

	/*
	 * Returns whether or not the ship is sunk
	 * @return true if ship is sunk, false otherwise
	 */
	bool sunk()
	{
		for (std::map<Coord, int>::iterator i = locs.begin(); i != locs.end(); i++)
		{
			if(i->second == 1)
			{
				return false;
			}
		}
		return true;
	}

	/*
	 * Hits specified coordinate of this ship
	 * @param c coordinate to hit this ship at
	 */
	void hit(Coord c)
	{
		if (!sunk())
		{
			locs[c] = 0;
			if (sunk())
			{
				std::cout << "SUNK " << name << "\n";
			}
			else
			{
				std::cout << "HIT\n";
			}
		}
		else
		{
			std::cout << "HIT\n";
		}
	}

	/*
	 * Moves the ship a certain amount in the x and a certain amount in the y direction
	 * @param coordinate coordinate that the ship is at
	 * @param xsteps spaces the ship should be moved in x direction
	 * @param ysteps spaces the ship should be moved in y direction
	 */
	void move(Coord coordinate, int xsteps, int ysteps)
	{
		front.first = front.first - xsteps;
		front.second = front.second - ysteps;
		back.first = back.first - xsteps;
		back.second = back.second - ysteps;
		std::map<Coord, int> newlocs;
		if (locs.find(coordinate) != locs.end())
		{
			for (std::map<Coord, int>::iterator it = locs.begin();
					it != locs.end(); it++)
			{
				int y = it->first.second;
				int x = it->first.first;
				int attacked = it->second;

				std::pair<int, int> oldcoordinate;
				oldcoordinate = std::make_pair(x, y);
				int newx = x - xsteps;
				int newy = y - ysteps;
				std::pair<int, int> newcoordinate;
				newcoordinate = std::make_pair(newx, newy);

				newlocs[newcoordinate] = attacked;
			}
			locs.swap(newlocs);
			std::cout << "MOVED\n";
		}

	}

	/*
	 * Returns vector of locations that the ship is currently occupying
	 * @return result resulting vector with all the coordinates that the ship is occupying
	 */
	std::vector<Coord> getLocsVector()
	{
		std::vector<Coord> result;
		for (std::map<Coord, int>::iterator i = locs.begin(); i != locs.end(); i++)
		{
			result.push_back(i->first);
		}
		return result;
	}

	/*
	 * Returns orientation of the ship
	 * @return orientation orientation of the ship
	 */
	char getOrientation()
	{
		return orientation;
	}

	/*
	 * Returns front of the ship, for horizontal ship, front is leftmost, for vertical it is topmost
	 * @return front front of the ship
	 */
	Coord getFront()
	{
		return front;
	}

	/*
	 * Returns back of the ship, for horizontal it is rightmost point, for veritcal it is bottommost point
	 * @return back back of the ship
	 */
	Coord getBack()
	{
		return back;
	}
};

#endif /* SHIP_HPP_ */
