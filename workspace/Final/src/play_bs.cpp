/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: final
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/
#include "play_bs.hpp"

typedef std::pair<int, int> Coord;
typedef std::pair<std::string, int> shipType;

using namespace std;

/*
 * Checks if an attack string is valid -must have 2 characters with both being digits
 * @param attack string to be checked
 * @return true if attack string is 2 digits, false otherwise
 */
bool checkValid(std::string attack)
{
	return attack.length() == 2 && isdigit(attack[0]) && isdigit(attack[1]);
}

/*
 * Checks if a coordinate does not have any ships on it
 * @param c coordinate to check if empty
 * @param ships vector of ships to check if they are positioned over the coordinate being checked
 * @return true if coordinate does not have a ship on it, false otherwise
 */
bool checkEmpty(Coord c, std::vector<Ship> ships)
{
	for (unsigned int i = 0; i < ships.size(); i++)
	{
		if (ships.at(i) == c && !ships.at(i).sunk())
		{
			return false;
		}
	}
	return true;
}

/*
 * Checks if input string is a valid movement command for battleship mobile. Command must be 4 characters
 * with the first, second and fourth being digits and third being u, l, d, r. The command cannot make the ship move
 * over an existing ship
 * @param move string to command movement of a ship
 * @param ships vector of ships that are currently on the board
 * @return true if input string is valid for mobile battleship, false otherwise
 */
bool checkValidM(std::string move, std::vector<Ship> ships)
{
	//Move input should be 4 characters
	if (move.length() != 4)
	{
		return false;
	}
	//First and second characters should be digits
	else if (!isdigit(move[0]) || !isdigit(move[1]))
	{
		return false;
	}
	//Third character should be one of u,d,l or r
	else if (tolower(move[2]) != 'u' && tolower(move[2]) != 'd' && tolower(move[2]) != 'l' && tolower(move[2]) != 'r')
	{
		return false;
	}
	//Fourth character should be a digit from 1 to 3 inclusive
	else if (!isdigit(move[3]) || (move[3] - 48 > 3 && move[3] - 48 < 1))
	{
		return false;
	}
	int x = move[0] - 48;
	int y = move[1] - 48;
	char m = move[2];
	int d = move[3] - 48;

	//Ship must exist at coordinate specified
	for (unsigned int i = 0; i < ships.size(); i++)
	{
		if (ships.at(i) == (make_pair(x, y)))
		{
			if (ships.at(i).getOrientation() == 'h')
			{
				if (m != 'l' && m!= 'r')
				{
					return false;
				}
				else if (m == 'l')
				{
					int leftmost = ships.at(i).getFront().first;
					for (int k = 1; k <= d; k++)
					{
						if(leftmost - k < 0)
						{
							return false;
						}
						if (!checkEmpty(make_pair(leftmost - k, y), ships))
						{
							return false;
						}
					}
				}
				else
				{
					int rightmost = ships.at(i).getBack().first;
					for (int k = 1; k <= d; k++)
					{
						if(rightmost + k > 9){
							return false;
						}
						if (!checkEmpty(make_pair(rightmost + k, y), ships))
						{
							return false;
						}
					}
				}
			}
			else
			{
				if (m != 'u' && m != 'd')
				{
					return false;
				}
				else if (m == 'u')
				{
					int topmost = ships.at(i).getFront().second;
					for(int k = 1; k <= d; k++)
					{
						if(topmost - k < 0){
							return false;
						}
						if (!checkEmpty(make_pair(x, topmost - k), ships))
						{
							return false;// issue here, need to be fixed, most likely issue with seeing its own piece
						}
					}
				}
				else
				{
					int bottommost = ships.at(i).getBack().second;
					for (int k = 1; k <= d; k++)
					{
						if(bottommost + k > 9){
							return false;
						}
						if (!checkEmpty(make_pair(x, bottommost + k), ships))
						{
							return false;
						}
					}
				}
			}
			return true;
		}
	}
	return false;
}

/*
 * Checks if the string is a valid placement command - string must be 3 characters with the first
 * two digits being digits and the last character either h or v. The coordinate must also not overlap with other ships.
 * @param placement string describing placement for ship
 * @param size size of ship to be placed
 * @param filled coordinates that are filled by ships
 * @return true if valid placement command, false otherwise
 */
bool checkValid(std::string placement, int size, std::vector<Coord> filled)
{
	//Position input should be 3 characters
	if (placement.length() != 3)
	{
		return false;
	}
	//First and second characters should be digits and last character is either 'h' or 'v'
	else if(!isdigit(placement[0]) || !isdigit(placement[1]) || !(placement[2] == 'h' ||  placement[2] == 'v'))
	{
		return false;
	}
	int x = placement[0] - 48;
	int y = placement[1] - 48;
	char o = placement[2];
	if (o == 'h')
	{
		if(x + size > 10)
		{
			return false;
		}
	}
	else
	{
		if(y + size > 10)
		{
			return false;
		}
	}
	for (std::vector<Coord>::iterator i = filled.begin(); i != filled.end(); i++)
	{
		if (o == 'h')
		{
			for (int k = 0; k < size; k++)
			{
				if(x + k == i->first && y == i->second)
				{
					return false;
				}
			}
		}
		else if (o == 'v')
		{
			for (int k = 0; k < size; k++)
			{
				if(x == i->first && y + k == i->second)
				{
					return false;
				}
			}
		}
	}
	return true;
}

/*
 * Creates a vector of ships for a specified player at position specified by the player
 * @param player player who will place ships on the board
 * @return ships vector of ships that have ships at player specified coordinates
 */
std::vector<Ship> placeShips(int player)
{
	std::vector<Coord> filledCoords;
	std::vector<Ship> ships;
	shipType shipTypes[5] =
		{
			make_pair("AIRCRAFT CARRIER", 5), make_pair("BATTLESHIP", 4),
			make_pair("CRUISER", 3), make_pair("SUBMARINE", 3), make_pair("DESTROYER", 2)
		};
		std::string placement;
		for (int i = 0; i < 5; )
		{
			std::cout << "PLAYER " << player << " PLACE " << shipTypes[i].first << ":" << std::endl;
			std::cin >> placement;
			if (placement == "q" || placement == "Q")
			{
				exit(1);
			}
			if (!checkValid(placement, shipTypes[i].second, filledCoords))
			{
				std::cout << "Invalid placement position" << std::endl;
			}
			else
			{
				int x = placement[0] - 48;
				int y = placement[1] - 48;
				Ship s = Ship(shipTypes[i].first, make_pair(x, y), shipTypes[i].second, placement[2]);
				ships.push_back(s);
				std::vector<Coord> locs = s.getLocsVector();

				for (int k = 0; k < s.size(); k++)
				{
					filledCoords.push_back(locs[k]);
				}
				i++;
			}
		}
		return ships;
}

/*
 * Begins battleship game, either normal or mobile
 * @param x not 1 for normal battleship, 1 for mobile battleship
 */
void playBattleship(int x)
{
	std::vector<Ship> p1ships = placeShips(1);
	std::vector<Ship> p2ships = placeShips(2);
	BattleshipGame bsg = BattleshipGame(p1ships, p2ships);
	GameResult result = RESULT_KEEP_PLAYING;
	while (result != RESULT_PLAYER1_WINS && result != RESULT_PLAYER2_WINS)
	{
		int player = bsg.getPlayer();
		std::string attack;
		//bsg.printBoard();
		std::cout << "PLAYER " << player << ":";
		std::cin >> attack;
		if (attack == "q")
		{
			std::exit(1);
		}
		else if(checkValid(attack))
		{
			result = bsg.attack_square(make_pair(attack[0] - 48, attack[1] - 48));
			bsg.switchPlayer();
		}
		else if(x == 1 && checkValidM(attack, bsg.getShips()))
		{
			bsg.moveship(make_pair(attack[0] - 48, attack[1] - 48), attack[2], attack[3]-48);
			bsg.switchPlayer();
		}
		else
		{

			std::cout << "INVALID MOVE\n";
		}
	}
	if(result == RESULT_PLAYER1_WINS)
	{
		std::cout << "PLAYER 1 WON\n";
	}
	else
	{
		std::cout << "PLAYER 2 WON\n";
	}
}

