/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: final
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/
#include <iostream>
//#include "battleship.hpp"
#include "play_bs.hpp"

int main()
{
	char selection;
	std::cout << "CHOOSE A GAME:" << std::endl;
	std::cin >> selection;
	if (selection == '1')
	{
		//battleship
		playBattleship(0);
	}
	else if (selection == '2')
	{
		//battleship mobile
		playBattleship(1);
	}
	else if (tolower(selection) == 'q')
	{
		exit(1);
	}
	else
	{
		std::cout << "Invalid game selected" << std::endl;
	}
	return 0;
}



