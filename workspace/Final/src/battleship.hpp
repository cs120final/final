/* Name: Kevin Peng, Jack Ye
Course number: 600.120
Date: Apr 29, 2016
Assignment number: final
Blackboard login: kpeng7, jye24
Email: kpeng7@jhu.edu, jye24@jhu.edu
*/
#ifndef BATTLESHIP_HPP_
#define BATTLESHIP_HPP_

#include <iostream>
#include <utility>
#include <array>
#include <vector>
#include "game.hpp"
#include "ship.hpp"

typedef std::pair<int, int> Coord;

class BattleshipGame : public Game
{
	std::vector<std::vector<Ship> > ships;
	int player1_numships;
	int player2_numships;
public:
	/*
	 * Default constructor for battleship game
	 */
	BattleshipGame() : Game(10, 10, 2)
	{
		player1_numships = 5;
		player2_numships = 5;
	}
	/*
	 * BattleshipGame constructor with arguments
	 */
	BattleshipGame(std::vector<Ship> ship1, std::vector<Ship> ship2) : Game(10, 10, 2)
	{
		ships.push_back(ship1);
		ships.push_back(ship2);
		player1_numships = 5;
		player2_numships = 5;
	}

	/*
	 * Changes the player that is playing
	 */
	void switchPlayer()
	{
		player = player % 2 + 1;
		currentBoard = b.at(player - 1);
	}

	/*
	 * Attempts to place marker at specified coordinate
	 * @param coordinate coordinate to place marker
	 * @return RESULT_INVALID invalid position to place the marker, or game is over
	 * @return RESULT_PLAYER_1_WINS player 1 wins the game
	 * @return RESULT_PLAYER_2_WINS player 2 wins the game
	 * @return RESULT_KEEP_PLAYING neither player has won and game will continue
	 */
	GameResult attack_square(Coord coordinate)
	{
		std::map<Coord, char> currentBoard = b.at(player - 1).getMap();
		if (!coorIsValid(coordinate))
		{
			return RESULT_INVALID;
		}
		else
		{
			if(player == 1)
			{
				for(int i = 0; i < player2_numships; i++)
				{
					if(ships.at(1).at(i) == coordinate)
					{
						ships.at(1).at(i).hit(coordinate);
						b.at(player - 1).addAt(coordinate, 'x');
						if (ships.at(1).at(i).sunk())
						{
							ships.at(1).erase(ships.at(1).begin() + i);
							player2_numships--;
							if(player2_numships == 0)
							{
								return RESULT_PLAYER1_WINS;
							}
							return RESULT_KEEP_PLAYING;
						}
						else
						{
							return RESULT_KEEP_PLAYING;
						}
					}
				}
				std::cout << "MISS\n";
				b.at(player - 1).addAt(coordinate, 'o');
				return RESULT_KEEP_PLAYING;
			}
			else
			{
				for(int i = 0; i < player1_numships; i++)
				{
					if(ships.at(0).at(i) == coordinate)
					{
						ships.at(0).at(i).hit(coordinate);
						b.at(player - 1).addAt(coordinate, 'x');
						if (ships.at(0).at(i).sunk())
						{
							ships.at(0).erase(ships.at(0).begin() + i);
							player1_numships--;
							if(player1_numships == 0)
							{
								return RESULT_PLAYER2_WINS;
							}
							return RESULT_KEEP_PLAYING;
						}
						else
						{
							return RESULT_KEEP_PLAYING;
						}
					}
				}
				std::cout << "MISS\n";
				b.at(player - 1).addAt(coordinate, 'o');
				return RESULT_KEEP_PLAYING;
			}
		}
		return RESULT_KEEP_PLAYING;
	}



/*
 * Moves ship at specified coordinate in the specified direction a certain amount of steps
 * @param coordinate coordinate of ship to move
 * @param direction direction to move ship
 * @param steps number of places to move
 */
	GameResult moveship(Coord coordinate, char direction, int steps)
	{
		if (direction == 'u')
		{
			//ships coordinates y values all add by steps
			if (player == 1)
			{
				for (int i = 0; i < player1_numships; i++)
				{
					if (ships.at(0).at(i) == coordinate)
					{
						ships.at(0).at(i).move(coordinate, 0, steps);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
			else
			{
				for (int i = 0; i < player2_numships; i++)
				{
					if (ships.at(1).at(i) == coordinate)
					{
						ships.at(1).at(i).move(coordinate, 0, steps);
						return RESULT_KEEP_PLAYING;
					}
				}
			}

		}
		if (direction == 'd')
		{
			//ships coordinates y values all subtract by steps
			if (player == 1)
			{
				for (int i = 0; i < 5; i++)
				{
					if (ships.at(0).at(i) == coordinate)
					{
						ships.at(0).at(i).move(coordinate, 0, -steps);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
			else
			{
				for (int i = 0; i < 5; i++)
				{
					if (ships.at(1).at(i) == coordinate)
					{
						ships.at(1).at(i).move(coordinate, 0, -steps);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
		}
		if (direction == 'r')
		{
			//ships coordinates x values all add by steps
			if (player == 1)
			{
				for (int i = 0; i < 5; i++)
				{
					if (ships.at(0).at(i) == coordinate)
					{
						ships.at(0).at(i).move(coordinate, -steps, 0);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
			else
			{
				for (int i = 0; i < 5; i++)
				{
					if (ships.at(1).at(i) == coordinate)
					{
						ships.at(1).at(i).move(coordinate, -steps, 0);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
		}
		if (direction == 'l')
		{
			//ships coordinates x values all subtract by steps
			if (player == 1)
			{
				for (int i = 0; i < 5; i++)
				{
					if (ships.at(0).at(i) == coordinate)
					{
						ships.at(0).at(i).move(coordinate, steps, 0);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
			else
			{
				for (int i = 0; i < 5; i++)
				{
					if (ships.at(1).at(i) == coordinate)
					{
						ships.at(1).at(i).move(coordinate, steps, 0);
						return RESULT_KEEP_PLAYING;
					}
				}
			}
		}
		return RESULT_INVALID;
	}

	/*
	 * Checks if given coordinate is valid
	 * @param coordinate coordinate to check validity for
	 * @return true if coordinate is valid
	 * @return false if coordinate is not valid
	 */
	bool coorIsValid(Coord coordinate)
	{
		return coordinate.first >= 0 && coordinate.first < 10 && coordinate.second >= 0 && coordinate.second < 10;
	}

	int getPlayer()
	{
		return player;
	}

	void printBoard()
	{
		currentBoard.print();
	}

	std::vector<Ship> getShips()
	{
		return ships.at(player - 1);
	}

	std::vector<Ship> getMyShips()
	{
		return ships.at((player % 2));
	}
};



#endif /* BATTLESHIP_HPP_ */
